FROM python:3.10
ARG SRCDIR=.
ARG PKGDIR="/pkg"

RUN echo "${PKGDIR}" && \
    mkdir -p ${PKGDIR}
WORKDIR ${PKGDIR}
COPY ${SRCDIR}/requirements.txt ${PKGDIR}/.
RUN python --version && \
    pip --version && \
    pip install -r ${PKGDIR}/requirements.txt
COPY ${SRCDIR} ${PKGDIR}
RUN pip install -e . && \
    mkdir -p "${PKGDIR}/checkpoints"
