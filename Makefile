TAG=gr/rotepy:test

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@grep -F -h "##" $(MAKEFILE_LIST) | grep -F -v grep -F | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: dbuild
dbuild: ## Build the testing docker container from local files
	podman build -t ${TAG} -f Dockerfile .

.PHONY: dtest
dtest: dbuild ## Test using container and unittest discover
	podman run -it ${TAG} python -m unittest discover -p "*_test.py" -s rotepy/

.PHONY: test
test: ## Test functionality plainly
	python -m unittest discover -p "*_test.py" -s rotepy/

.PHONY: drun
drun: dbuild ## Run the testing container interactively
	podman run --entrypoint bash -it ${TAG}

