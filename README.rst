RotePy
======

Probably the most basic, brain dead, operations abstracted so that we dont have to do them over and over again for every machine learning model.

This is simply an abstraction that can take in abitrary pytorch models and dataloaders to produce useful outputs with as minimal BS inbetween. I was simply very bored of doing the same things every time so I abstracted it and posted it here to share.
