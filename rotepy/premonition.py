import torch
import copy
import numpy as np
from rotepy.encoding import PositionalEncoding, ItPositionalEncoding

# NEURAL NETWORK

# Some helpful sources
# https://proceedings.neurips.cc/paper/2017/file/3f5ee243547dee91fbd053c1c4a845aa-Paper.pdf
# https://jalammar.github.io/illustrated-transformer/
# https://medium.com/mlearning-ai/transformer-implementation-for-time-series-forecasting-a9db2db5c820
# https://www.youtube.com/watch?v=dichIcUZfOw
# https://github.com/CVxTz/time_series_forecasting/blob/main/time_series_forecasting/model.py
# https://towardsdatascience.com/how-to-use-transformer-networks-to-build-a-forecasting-model-297f9270e630
# https://www.youtube.com/watch?v=1biZfFLPRSY

class Premonition(torch.nn.Module):
    """Tri-input multi-modal yield forecasting model."""

    def __init__(self,
                 shape: tuple,
                 batch_size: int = 32,
                 ):
        """Initialise network architecture.
        :args:

            - shape: the tuple shape of any one timeline
              (they should all match so pick any one).
            - batch_size: the number of examples to process at once.
        """
        super().__init__()
        self.model_type = "Transformer"
        self.past_model = Timeline(shape)
        self.present_model = Timeline(shape)
        self.future_model = Timeline(shape)
        self.batch_size = batch_size
        self.input_shape = shape

        self.merge_model = torch.nn.Linear(
                in_features=shape[-1]*3,
                out_features=shape[-1])

        self.dense_model = torch.nn.Linear(
                in_features=shape[-1],
                out_features=1)

    def forward(self, past, present, future, y):
        """Calculate forward pass."""
        expanded_y = []
        # take y of shape (32,) to (32, 1, 51)
        for i in y:
            expanded_y.append(torch.broadcast_to(i, (1, self.input_shape[-1])))
        expanded_y = torch.stack(expanded_y)
        past_out = self.past_model.forward(past, expanded_y)
        past_out = torch.reshape(past_out, (past.shape[0], -1))
        pres_out = self.present_model.forward(present, expanded_y)
        pres_out = torch.reshape(pres_out, (present.shape[0], -1))
        futu_out = self.future_model.forward(future, expanded_y)
        futu_out = torch.reshape(futu_out, (future.shape[0], -1))
        activations = torch.cat((past_out, pres_out, futu_out), 1)
        # bring them together
        merge_out = self.merge_model.forward(activations)

        # now squash
        y_hat = self.dense_model.forward(merge_out)
        return torch.flatten(y_hat)


class Timeline(torch.nn.Module):
    """NeuralNnetwork concerned with processing a timeline."""

    def __init__(self,
                 shape,
                 batch_size: int = 32,
                 max_seq_length: int = None,
                 **kwargs):
        """Timeline processes something like past present future.

        :args:

            - shape: the tuple shape of any one timeline
              (they should all match so pick any one).
            - batch_size: the number of examples to process at once.
            - **kwargs: additional kword args to provide to nn.
        """
        super().__init__()
        self.model_type = "Transformer"
        self.shape = shape
        self.maximum_sequence_length = max_seq_length if max_seq_length is \
            not None else self.shape[-2]

        # creating defaults to be overriding with kwargs
        defaults = {
                "d_model": self.shape[-1],
                "nhead": 5,
                "num_encoder_layers": 12,
                "batch_first": True,
                # num_decoder_layers: 6,  # number of sub decoder layers
                "dim_feedforward": 2048,  # dimensions of feedforward net
                "dropout": 0.1,           # the dropout value
                "activation": "gelu",     # activation function of encoder
                # custom_encoder: None,   # custom encoder
                # custom_decoder: None,   # custom decoder
                # layer_norm_eps: 1e-5,   # eps value in layer normalization
            }
        self.subargs = self._mergeDicts(defaults, kwargs)

        self.positional_encoder = ItPositionalEncoding(
                d_model=self.subargs["d_model"],
                max_len=self.maximum_sequence_length)
        # https://github.com/pytorch/pytorch/blob/master/torch/nn/modules/transformer.py
        self.transformer_model = torch.nn.Transformer(**self.subargs)

    def forward(self, x, y):
        """Neural network forward prop."""
        # by default we might expect the input shapes to be:
        # (batches=32, sequence_len=10081, fetures=51) Past
        # (batches=32, sequence_len=8065, fetures=51) Present
        # (batches=32, sequence_len=2017, fetures=51) Premonition
        # src = torch.rand((batch_size, len(sample[0]), self.d_model))
        # tgt = torch.rand((batch_size, 1, self.d_model))

        # we need to block the decoder from cheating with an input mask and
        # shifting the target values by 1
        src_seq_len = y.shape[-2]
        tgt_mask = (torch.triu(torch.ones((src_seq_len, src_seq_len)) == 1))
        tgt_mask = tgt_mask.transpose(0,1).float()
        tgt_mask = tgt_mask.masked_fill(tgt_mask == 0, True)
        tgt_mask = tgt_mask.masked_fill(tgt_mask == 1, False)
        y_prep = torch.roll(y, 1, -2)
        for i in range(y.shape[0]):
            # set the first example (1, self.d_model) as zero
            # this is broadcasting to the correct shape
            y_prep[i, 0] = False
        # encoding positions using waves
        pos_encoded = self.positional_encoder.forward(x)
        # forward prop a single sequence
        transformered = self.transformer_model.forward(
                tgt=y_prep,
                src=pos_encoded,
                tgt_mask=tgt_mask)
        return transformered

    def _mergeDicts(self, *dicts):
        """Given multiple dictionaries, deep copy, merge together in order."""
        dicts = copy.deepcopy(dicts)
        result = {}
        for dictionary in dicts:
            result.update(dictionary)  # merge each dictionary in order
        return result

    _mergeDicts.__annotations__ = {"dicts": dict, "return": dict}
