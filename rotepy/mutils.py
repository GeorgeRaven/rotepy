# https://stackoverflow.com/a/33046935
def dl_to_ld(dl: dict):
    """Converts a dictionary of lists to a list of dictionaries."""
    return [dict(zip(dl, t)) for t in zip(*dl.values())]

def ld_to_dl(ld: list):
    """Converts a list of dictionaries to a dictionary of lists."""
    return {k: [dic[k] for dic in ld] for k in ld[0]}
