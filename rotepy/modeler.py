# Modeler: machine learning modeling abstraction to reduce mental complexity.

# inbuilt imports
import os
import tqdm
import time
import pathlib
import logging as logger
import datetime
import importlib
import collections

# 3rd party library imports
import torch
from torch.utils.tensorboard import SummaryWriter
from torchmetrics.regression import MeanAbsolutePercentageError, WeightedMeanAbsolutePercentageError
from torchinfo import summary
import numpy as np
import pandas as pd
import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt

# local imports
import rotepy.style
from rotepy.premonition import Premonition
from rotepy.checkpoint import model_checkpoint, model_checkpoint_load
from rotepy.mutils import dl_to_ld, ld_to_dl

# ___________________________________  
# | _____ |   | ___ | ___ ___ | |   | |
# | |   | |_| |__ | |_| __|____ | | | |
# | | | |_________|__ |______ |___|_| |
# | |_|   | _______ |______ |   | ____|
# | ___ | |____ | |______ | |_| |____ |
# |___|_|____ | |   ___ | |________ | |
# |   ________| | |__ | |______ | | | |
# | | | ________| | __|____ | | | __| |
# |_| |__ |   | __|__ | ____| | |_| __|
# |   ____| | |____ | |__ |   |__ |__ |
# |_|_______|_______|___|___|__ |_____|

class Modeler:

    ###################
    # .0 OBJECT RELATED
    ###################

    def __init__(self):
        """Modeler: modeling neural networks and their training.

        It is expected that certain properties of this class exist at the point in time
        that functions are called. They will assert this in the first instance, so you
        will know if you are missing properties being set.
        """
        self.writer = SummaryWriter()

    #################
    # 1. DATA RELATED
    #################

    @property
    def dataloader_training(self):
        """Dataloader for training data during training."""
        out = self.__dict__.get("_dl_t")
        if out is None:
            raise ValueError(
                    "Training dataloader is not assigned.")
        return out

    @dataloader_training.setter
    def dataloader_training(self, dataloader):
        """"Set the dataloader for training."""
        self._dl_t = dataloader

    @property
    def dataloader_validation(self):
        """Dataloader for validation during training."""
        out = self.__dict__.get("_dl_v")
        if out is None:
            raise ValueError(
                    "Validation dataloader is not assigned.")
        return out

    @dataloader_validation.setter
    def dataloader_validation(self, dataloader):
        """Set the dataloader for validation during training."""
        self._dl_v = dataloader

    @property
    def dataloader_testing(self):
        """Dataloader for testing/ inference data."""
        out = self.__dict__.get("_dl_i")
        if out is None:
            raise ValueError(
                    "Inference/ testing dataloader is not assigned.")
        return out

    @dataloader_testing.setter
    def dataloader_testing(self, dataloader):
        """Set the dataloader for testing data."""
        self._dl_i = dataloader

    ##################
    # 2. MODEL RELATED
    ##################

    @property
    def model(self):
        """Model with which to train or infer with."""
        out = self.__dict__.get("_model")
        if out is None:
            raise ValueError(
                    "Model is not assigned.")
        return out

    @model.setter
    def model(self, model):
        self._model = model

    @property
    def model_args(self):
        """Args necessary to pass to model to recreate it."""
        out = self.__dict__.get("_model_kwargs")
        if out is None:
            raise ValueError(
                    "Model keyword args not assigned.")
        return out

    @model_args.setter
    def model_args(self, kwargs):
        """Set additional model args required to recreate it."""
        assert isinstance(kwargs, dict)
        self._model_kwargs = kwargs

    @property
    def model_path(self):
        """Path where model checkpoint should be/ will be generated."""
        out = self.__dict__.get("_model_path")
        if out is None:
            raise ValueError(
                    "Model checkpoint path not assigned.")
        return out

    @model_path.setter
    def model_path(self, path):
        """Set model checkpoint path."""
        self._model_path = os.path.abspath(path)
        if os.path.isfile(self._model_path):
            self.load_checkpoint()


    ######################
    # 3. OPTIMIZER RELATED
    ######################

    @property
    def optimizer(self):
        """Optimizer with which to update the weights of the neural network."""
        out = self.__dict__.get("_optimizer")
        if out is None:
            raise ValueError(
                    "Optimizer is not assigned.")
        return out

    @optimizer.setter
    def optimizer(self, optimizer):
        """Set the model optimizer for weight optimization."""
        self._optimizer = optimizer

    @property
    def optimizer_args(self):
        """Arguments necessary to recreate the optimizer."""
        out = self.__dict__.get("_optimizer_kwargs")
        if out is None:
            raise ValueError(
                    "Optimizer additional keyword arguments not assigned.")
        return out

    @optimizer_args.setter
    def optimizer_args(self, kwargs):
        """Set optimizer additional args for recreating the optimizer object."""
        assert isinstance(kwargs, dict)
        self._optimizer_kwargs = kwargs

    @property
    def scheduler(self):
        """Scheduler with which to update the LR of the neural network."""
        out = self.__dict__.get("_scheduler")
        if out is None:
            raise ValueError(
                    "Scheduler is not assigned.")
        return out

    @scheduler.setter
    def scheduler(self, scheduler):
        """Set the model scheduler for weight optimization."""
        self._scheduler = scheduler

    @property
    def scheduler_args(self):
        """Arguments necessary to recreate the scheduler."""
        out = self.__dict__.get("_scheduler_kwargs")
        if out is None:
            raise ValueError(
                    "Scheduler additional keyword arguments not assigned.")
        return out

    @scheduler_args.setter
    def scheduler_args(self, kwargs):
        """Set scheduler additional args for recreating the scheduler object."""
        assert isinstance(kwargs, dict)
        self._scheduler_kwargs = kwargs

    ##########################
    # 4. LOSS FUNCTION RELATED
    ##########################

    @property
    def loss_function(self):
        """Loss function with which to calculate our level of wrongness during training."""
        out = self.__dict__.get("_loss_function")
        if out is None:
            raise ValueError(
                    "Loss function OBJECT not assigned.")
        return out

    @loss_function.setter
    def loss_function(self, loss_fn):
        """Set the loss function we will use for calculating wrongness."""
        self._loss_function = loss_fn


    @property
    def loss_function_args(self):
        """Arguments necessary to recreate the loss function."""
        out = self.__dict__.get("_loss_function_kwargs")
        if out is None:
            raise ValueError(
                    "Loss function generating, keyword arguments not assigned.")
        return out

    @loss_function.setter
    def loss_function_args(self, kwargs):
        """Set the keyword arguments necessary to rebuild the loss function."""
        assert isinstance(kwargs, dict)
        self._loss_function_kwargs = kwargs

    @property
    def history(self):
        if self.__dict__.get("_history") is None:
            self._history = {
                        "loss_training": [],
                        "loss_validation": [],
                        "loss_testing": [],
                    }
        return self._history

    @history.setter
    def history(self, d):
        self._history = d

    #########
    # 5. MISC
    #########

    @property
    def device(self):
        out = self.__dict__.get("_device")
        if out is None:
            out = "cuda" if torch.cuda.is_available() else "cpu"
            self.device = out
        return out

    @device.setter
    def device(self, device):
        self._device = device

    @property
    def metadata(self):
        out = self.__dict__.get("_facebook")
        if out is None:
            raise ValueError(
                    "Loss function generating, keyword arguments not assigned.")
        return out

    @metadata.setter
    def metadata(self, kwargs):
        """Set the residual metadata."""
        assert isinstance(kwargs, dict)
        self._facebook = kwargs

    #####################################
    # 6. MODEL TRAINING / TESTING RELATED
    #####################################

    @property
    def epoch(self):
        out = self.__dict__.get("_epoch")
        if out is None:
            self._epoch = 0
        return self.__dict__.get("_epoch")

    @epoch.setter
    def epoch(self, epoch):
        self._epoch = epoch

    def retrain(self,
            desired_epoch=None, added_epoch=None,
            to_pass_target=None,
            device=None,
            validation_split=None):

        logger.info("resetting optimizer")
        # OPTIMIZERS
        # generate instance of optimizer
        optMod = importlib.import_module(str(self.optimizer.__class__.__module__))
        optClass = getattr(optMod, str(self.optimizer.__class__.__name__))
        optimizer = optClass(self.model.parameters(), **self.optimizer_args)
        self.optimizer = optimizer

        self.train(
            desired_epoch=desired_epoch, added_epoch=added_epoch,
            to_pass_target=to_pass_target,
            device=device,
            validation_split=validation_split)

    # ---

    def train(self,
            desired_epoch=None, added_epoch=None,
            to_pass_target=None,
            device=None,
            validation_split=None):
        """Train model on training dataloader in batches, for a set number of epochs.

        :arg desired_epoch: The sum total epoch we want the model to be trained until, mutually exclusive with added_epoch.
        :type desired_epoch: int
        :arg added_epoch: The number of additional epochs to run on top of however many the model already has.
        :type added_epoch: int
        :arg to_pass_target: Whether or not the model needs some form of target to be passed in, e.g transformer decoders.
        :type to_pass_target: bool
        :arg device: The name of the device to put the model and data on to (cpu | gpu)
        :type device: string
        :arg validation_split: The decimal proportion of the data that should be used in the validation set.
        :type validation_split: float
        """
        if desired_epoch is None and added_epoch is None:
            raise ValueError("You have not given modeler.train a target epoch or an additional epoch.")
        to_pass_target = to_pass_target if to_pass_target is not None else False
        device = device if device is not None else self.device
        validation_split = validation_split if validation_split is not None else 0.2

        start_epoch = int(self.epoch)
        final_epoch = desired_epoch if desired_epoch is not None else self.epoch + added_epoch
        dataloader_training = self.dataloader_training
        dataloader_validation = self.dataloader_validation
        dataloader_testing = self.dataloader_testing
        model = self.model.to(device)
        model.to(device)
        loss_fn = self.loss_function.to(device)
        lowest_loss_test = 9e+10
        optimizer = self.optimizer
        scheduler = self.scheduler

        # setting epoch and batch tqdm up
        epoch_bar = tqdm.tqdm(range(self.epoch, final_epoch, 1), position=0, leave=True,
                        desc="epoch", colour="blue")
        # store for losses so they can be later plotted
        loss_training = []
        loss_validation = []
        loss_testing = []


        # now for actual training iterating over epochs and batches
        for epoch in epoch_bar:
            batch_bar = tqdm.tqdm(total=len(dataloader_training), position=1, leave=False,
                            desc="trn_set", colour="green")
            test_bar = tqdm.tqdm(total=len(dataloader_testing), position=2, leave=False,
                            desc="tst_set", colour="magenta")

            batch_losses_training = []
            batch_losses_validation = []
            batch_losses_testing = []
            split_id = len(batch_bar) - np.ceil(len(batch_bar) * validation_split)
            y_hat_list = []
            y_list = []

            # TRAINING SET
            # we split the training set by batch_id into subsets for training and validation
            # training should always occur first, and by virtue of the dataloader shuffling
            # the higher b_ids will be randomly selected folds as we desire and occur later
            # we simply call evaluation methods instead of training methods
            for b_id, data in enumerate(dataloader_training):
                data = [i.to(device) for i in list(data)]

                if epoch == 0 and b_id == 0:
                    #torch.onnx.export(model,
                    #                  tuple(data),
                    #                  "mymodel.onnx",
                    #                  export_params=True,
                    #)
                    summary(model, input_data=list(data))
                    self.writer.add_graph(model, data)
                # the end of data may contain a metadata dictionary
                # if we find such a dictionary then we split it from data
                if isinstance(data[-1], dict):
                    # we found a dictionary split it
                    data, b_meta = data[:-1], data[-1] 
                if b_id < split_id:
                    # TRAINING
                    loss_item_train, pred = self.train_single_batch(
                            model,
                            loss_fn,
                            optimizer,
                            *data,
                            to_pass_target=to_pass_target, 
                            device=device)

                    y_hat_list.append(pred.detach())
                    y_list.append(data[-1].detach())
                    # SAVING TRAINING HISTORY AND LOGGING
                    batch_losses_training.append(loss_item_train)
                    batch_bar.set_description("trn_set:{},trn:{},std:{}".format(b_id,
                        round(loss_item_train, 4), torch.round(torch.std(pred), decimals=4) ))
                    # saving various metrics to disk
                    self.writer.add_scalar("Loss/train", loss_item_train, epoch)
                    self.write_loss_stats(prefix="Loss/train", epoch=epoch, y=data[-1], y_hat=pred)
                    self.write_y_stats(prefix="Stats/train", epoch=epoch, y=data[-1], y_hat=pred)
                else:
                    # VALIDATION
                    loss_item_validation, pred = self.test_single_batch(
                            model,
                            loss_fn,
                            *data,
                            to_pass_target=to_pass_target,
                            device=device) 

                    y_hat_list.append(pred.detach())
                    y_list.append(data[-1].detach())
                    # SAVING VALIDATION HISTORY AND LOGGING
                    batch_losses_validation.append(loss_item_validation)
                    batch_bar.set_description("trn_set:{},val:{},std:{}".format(b_id,
                        round(loss_item_validation, 4), torch.round(torch.std(pred), decimals=4) ))
                    #for name, module in model.named_children():
                    #    parameters = module.parameters()
                    #    norm = torch.norm(
                    #        torch.stack([torch.norm(p.grad.detach(), 2) for p in parameters]), 2)
                    #    self.writer.add_scalar(f'Grads/{name}', norm, epoch)
                    self.writer.add_scalar("Loss/validation", loss_item_validation, epoch)
                    self.write_loss_stats(prefix="Loss/validation", epoch=epoch, y=data[-1], y_hat=pred)
                    self.write_y_stats(prefix="Stats/validation", epoch=epoch, y=data[-1], y_hat=pred)
                batch_bar.update(1)
            #self.writer.add_graph(model, data)
            #before_lr = optimizer.param_groups[0]["lr"]
            self.writer.add_scalar("Optimizer/LR", optimizer.param_groups[0]["lr"], epoch)
            scheduler.step(metrics=np.array(batch_losses_validation).mean())
            #after_lr = optimizer.param_groups[0]["lr"]
            #print("Epoch %d: lr %.4f -> %.4f" % (epoch, before_lr, after_lr))

            # ensuring metrics written to disk
            self.writer.flush()

            # TESTING SET
            for t_id, test_data in enumerate(dataloader_testing):
                test_data = [i.to(device) for i in list(test_data)]
                # the end of data may contain a metadata dictionary
                # if we find such a dictionary then we split it from data
                if isinstance(test_data[-1], dict):
                    # we found a dictionary split it
                    test_data, t_meta = test_data[:-1], test_data[-1]
                # EVALUATION
                loss_item_testing, pred = self.test_single_batch(
                        model,
                        loss_fn,
                        *test_data,
                        to_pass_target=to_pass_target,
                        device=device) 

                # SAVING TESTING HISTORY AND LOGGING
                y_hat_list.append(pred.detach())
                y_list.append(test_data[-1].detach())
                batch_losses_testing.append(loss_item_testing)
                batch_bar.set_description("batch:{},tst:{},std:{}".format(b_id,
                    round(loss_item_testing, 4), torch.round(torch.std(pred), decimals=4) ))
                self.writer.add_scalar("Loss/test", loss_item_testing, epoch)
                self.write_loss_stats(prefix="Loss/test", epoch=epoch, y=test_data[-1], y_hat=pred)
                self.write_y_stats(prefix="Stats/test", epoch=epoch, y=test_data[-1], y_hat=pred)
                test_bar.update(1)

            y = torch.reshape(torch.cat(y_list), (-1,))
            y_hat = torch.reshape(torch.cat(y_hat_list), (-1,))
            self.writer.add_figure("Forecasted vs. Target", self.plot_y_y_hat_graph(y.cpu(),y_hat.cpu()), epoch)  # Log graph
            plt.close()

            self.writer.flush()

            # AVERAGES AND EXTENDING HISTORY
            avg_loss_train = np.average(batch_losses_training)
            self.history["loss_training"].append(avg_loss_train)

            avg_loss_val = np.average(batch_losses_validation)
            self.history["loss_validation"].append(avg_loss_val)

            avg_loss_test = np.average(batch_losses_testing)
            self.history["loss_testing"].append(avg_loss_test)

            self.epoch = self.epoch + 1

            epoch_bar.set_description("epoch:{},trn:{},val:{},tst:{}".format(self.epoch,
                round(avg_loss_train, 4),
                round(avg_loss_val, 4),
                round(avg_loss_test, 4),
                ))

            # MODEL CHECKPOINTING
            if avg_loss_test < lowest_loss_test:
                self.save_checkpoint()
                lowest_loss_test = avg_loss_test

            self.plot()

    def write_loss_stats(self, prefix, epoch, y, y_hat):
        """Attempt to log loss related statistics for tensorboard.
        
        This may not work for categorical outputs but it will ignore such errors.
        """
        try:
            with torch.no_grad():
                # MAE
                mae = torch.nn.L1Loss()(y_hat, y)
                self.writer.add_scalar(f"{prefix}/MAE", mae, epoch)
                # MSE
                mse = torch.nn.MSELoss()(y_hat, y)
                self.writer.add_scalar(f"{prefix}/MSE", mse, epoch)
                # RMSE
                rmse = torch.sqrt(mse)
                self.writer.add_scalar(f"{prefix}/RMSE", rmse, epoch)
                # MAPE
                metric = MeanAbsolutePercentageError().to(y.device)
                mape = metric(y_hat, y)
                self.writer.add_scalar(f"{prefix}/MAPE", mape, epoch)
                # WAPE
                metric = WeightedMeanAbsolutePercentageError().to(y.device)
                wape = metric(y_hat, y)
                self.writer.add_scalar(f"{prefix}/WAPE", wape, epoch)
        except RuntimeError as e:
            # this should never fail even if we cant use these losses on this problem
            # e.g cant use MAE in place of categorical cross entropy loss
            print(e)

    def write_y_stats(self, prefix, epoch, y, y_hat):
        """Log descriptors and statistics for both y and y_hat so we can get a sense for the distribution."""
        # STD
        std = torch.std(y_hat.float())
        self.writer.add_scalar(f"{prefix}/STD/y_hat", std, epoch)
        std = torch.std(y.float())
        self.writer.add_scalar(f"{prefix}/STD/y", std, epoch)

    def train_single_batch(self, model, loss_function, optimizer, *data, to_pass_target=None, device=None):
        device = device if device is not None else model.device
        to_pass_target = to_pass_target if to_pass_target is not None else False
        data = [i.to(device) for i in list(data)]
        model.train()
        # COMPUTE FORWARD ERROR
        model.zero_grad()
        optimizer.zero_grad()
        pred = model(*data if to_pass_target is True else data[:-1])
        loss = loss_function(pred, data[-1])
        # BACKPROP
        loss.backward()
        optimizer.step()

        return loss.item(), pred

    def test(self,
            to_pass_target=None,
            device=None,
            emit_y=None,
            emit_metadata=None):
        """Test apply model on testing dataloader to get some predictions.

        :arg to_pass_target: Whether or not the model needs some form of target to be passed in, e.g transformer decoders.
        :type to_pass_target: bool
        :arg device: The name of the device to put the model and data on to (cpu | gpu)
        :type device: string
        :arg emit_metadata: Whether or not to emit the metadata at the end of the returned tuple
        :type emit_metadata: bool
        """
        to_pass_target = to_pass_target if to_pass_target is not None else False
        device = device if device is not None else "cpu"
        emit_y = emit_y if emit_y is not None else False
        emit_metadata = emit_metadata if emit_metadata is not None else False

        dataloader_testing = self.dataloader_testing
        model = self.model.to(device)
        loss_fn = self.loss_function.to(device)

        # setting epoch and batch tqdm up
        epoch_bar = tqdm.tqdm(range(self.epoch, self.epoch + 1, 1), position=0, leave=True,
                        desc="epoch", colour="blue")
        # store for losses so they can be later plotted
        loss_testing = []

        # now for actual training iterating over epochs and batches
        for epoch in epoch_bar:
            test_bar = tqdm.tqdm(dataloader_testing, position=1, leave=False,
                            desc="tst_set", colour="magenta")

            batch_losses_testing = []
            actuals = []
            predictions = []
            epoch_metadata = []

            # TESTING SET
            for t_id, data in enumerate(test_bar):
                data = [i.to(device) for i in list(data)]
                # the end of data may contain a metadata dictionary
                # if we find such a dictionary then we split it from data
                if isinstance(data[-1], dict):
                    # we found a dictionary split it
                    data, t_meta = data[:-1], data[-1] 
                    # dataloaders create a dict of lists we want a list of dicts
                    # so that the index matches output value
                    # https://stackoverflow.com/a/33046935
                    epoch_metadata.extend(dl_to_ld(t_meta))
                else:
                    # appending a dictionary even if
                    epoch_metadata.extend([dict()]*len(data[0]))
                if emit_y:
                    actuals.extend(data[-1])
                # EVALUATION
                loss_item_testing, pred = self.test_single_batch(
                        model,
                        loss_fn,
                        *data,
                        to_pass_target=to_pass_target,
                        device=device) 
                # STORING RESULT
                batch_losses_testing.append(loss_item_testing)
                predictions.extend(pred)

            # AVERAGES AND EXTENDING HISTORY
            avg_loss_test = np.average(batch_losses_testing)
            # self.history["loss_testing"].append(avg_loss_test)

            self.epoch = self.epoch + 1

            epoch_bar.set_description("epoch:{},tst{}".format(self.epoch,
                round(avg_loss_test, 4)))

            self.plot()
            out = (torch.stack(predictions).cpu(),)
            out = out + (actuals.cpu(),) if emit_y else out
            out = out + (epoch_metadata.cpu(),) if emit_metadata else out
            return out

    def test_single_batch(self, model, loss_function, *data, to_pass_target=None, device=None):
        device = device if device is not None else model.device
        to_pass_target = to_pass_target if to_pass_target is not None else False
        data = [i.to(device) for i in list(data)]
        model.eval()
        with torch.no_grad():
            pred = model(*data if to_pass_target is True else data[:-1])
            loss_item = loss_function(pred, data[-1]).item()
        return loss_item, pred

    def test_trace(self,
                 to_pass_target=None,
                 device=None):
        """Test_trace infers and gathers metadata to reverse dataloader data.
        
        This function will be particularly important for showcasing and tracing
        outputs. It will give you for every example over the test dataloader:

        - normalised y
        - normalised y_hat
        - example index

        :arg to_pass_target: Whether or not the model needs some form of target to be passed in, e.g transformer decoders.
        :type to_pass_target: bool
        :arg device: The name of the device to put the model and data on to (cpu | gpu)
        :type device: string
        """
        return self.test(to_pass_target=to_pass_target, device=device, emit_y=True, emit_metadata=True)


    ###############################
    # 7. MODEL SAVING AND RELOADING
    ###############################

    def save_checkpoint(self, **extra):
        cp = {
                "datetime": datetime.datetime.utcnow(),
                "manager": "rotepy",
                "epoch": self.epoch,
                "history": self.history,

                "model_state_dict": self.model.state_dict(),
                "model_module": str(self.model.__class__.__module__),
                "model_class": str(self.model.__class__.__name__),
                "model_args": self.model_args,

                "optimizer_state_dict": self.optimizer.state_dict(),
                "optimizer_module": str(self.optimizer.__class__.__module__),
                "optimizer_class": str(self.optimizer.__class__.__name__),
                "optimizer_args": self.optimizer_args,

                "scheduler_state_dict": self.scheduler.state_dict(),
                "scheduler_module": str(self.scheduler.__class__.__module__),
                "scheduler_class": str(self.scheduler.__class__.__name__),
                "scheduler_args": self.scheduler_args,

                "loss_module": str(self.loss_function.__class__.__module__),
                "loss_class": str(self.loss_function.__class__.__name__),
                "loss_function_args": self.loss_function_args,

                **extra
        }
        pathlib.Path(self.model_path).parent.mkdir(parents=True, exist_ok=True)
        torch.save(cp, self.model_path)

    def load_checkpoint(self):
        cp = torch.load(self.model_path)

        # if model args exist use them, if not use the saved models args
        # please ensure you arent wrongly trying to set model args
        try:
            self.model_args
        except ValueError as e:
            self.model_args = cp["model_args"]

        # print(cp.keys())
        self.epoch = cp["epoch"]
        logger.info("model args {}".format(self.model_args))
        self.optimizer_args = cp["optimizer_args"]
        self.history = cp["history"]
        # TODO re-enable loss function args
        self.loss_function_args = {} # cp["loss_function_args"]

        # Modules are loaded dynamically based on system availability.
        # We save the module and classes used then reload them here.
        # I dont like this solution but behind the scenes other libs do the same.
        # In particular pickle saves a filepath to module which breaks between systems.
        # We dont do this, we attempt to look it up based on the running system.

        # MODELS
        # generate instance of model
        modelMod = importlib.import_module(cp["model_module"])
        modelClass = getattr(modelMod, cp["model_class"])
        model = modelClass(**self.model_args)
        model.load_state_dict(cp["model_state_dict"])
        self.model = model.to(self.device)

        # OPTIMIZERS
        # generate instance of optimizer
        optMod = importlib.import_module(cp["optimizer_module"])
        optClass = getattr(optMod, cp["optimizer_class"])
        optimizer = optClass(model.parameters(), **cp["optimizer_args"])
        optimizer.load_state_dict(cp["optimizer_state_dict"])
        self.optimizer = optimizer

        # SCHEDULERS
        # generate instance of scheduler
        schMod = importlib.import_module(cp["scheduler_module"])
        schClass = getattr(schMod, cp["scheduler_class"])
        scheduler = schClass(optimizer, **cp["scheduler_args"])
        scheduler.load_state_dict(cp["scheduler_state_dict"])
        self.scheduler = scheduler

        # LOSS FUNCTIONS
        # attain loss function
        losMod = importlib.import_module(cp["loss_module"])
        losClass = getattr(losMod, cp["loss_class"])
        loss_fn = losClass()
        self.loss_function = loss_fn.to(self.device)

    ###################
    # 8. MODEL PLOTTING
    ###################

    def plot(self):
        """Plot the train, test, validation graph.

        This will get the models history for training, validation, and testing and plot them using seaborn.
        """
        yr = datetime.datetime.utcnow().date().year
        ed = {
                "training": self.history["loss_training"],
                "validation": self.history["loss_validation"],
                "testing": self.history["loss_testing"],
        }
        data = {}
        for k, v in ed.items():
            data[k] = np.array(v) # np.sqrt(np.array(v))
        data = pd.DataFrame(data)

        plt.figure()
        ax = sns.lineplot(data, legend="brief")
        ax.set(
            title="Train-Test-Validation Loss Over Epochs Line Graph",
            xlabel="Epoch",
            ylabel="Loss ({})".format(str(self.loss_function)),
        )
        plt.legend(title="Data Set",
                   loc="upper right")
        #plt.text(x=0, y=0.1, s="© {} George Onoufriou".format(yr))
        fig = ax.get_figure()
        fig.savefig("{}.svg".format(self.model_path), bbox_inches="tight")
        fig.savefig("{}.png".format(self.model_path), bbox_inches="tight")
        fig.savefig("{}.pdf".format(self.model_path), bbox_inches="tight")
        plt.close()

    def plot_y_y_hat_graph(self, y, y_hat):
        plt.figure()
        fig, ax = plt.subplots()
        ax = sns.lineplot(data=y, label="Target")
        ax = sns.lineplot(data=y_hat, label="Forecasted")
        ax.set(xlabel="Instance", ylabel="Value")
        ax.set_title("Target vs. Forecasted")
        ax.legend()
        plt.ylim([-0.2, 1.2])
        return fig
