class LinearNormal():
    r"""LinearNormal is a normalisation utility between arbitrary range [a,b]."""

    def __init__(self, mn, mx):
        r"""Instantiate LinearNormal with default ranges.

        :arg mn: Minimum expected value stored and used throughout the lifetime of the class

        :arg mx: Maximum expected value stored and used throughout the lifetime of the class

        :example: LinearNormal(mn=-1, mx=1)
        """
        self.mn = mn
        self.mx = mx

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, state):
        self.__dict__ = state

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.__dict__)

    @property
    def mn(self):
        """Minimum observed value"""
        return self.__dict__.get("_mn")

    @mn.setter
    def mn(self, mn):
        self._mn = mn

    @property
    def mx(self):
        """Maximum observed value"""
        return self.__dict__.get("_mx")

    @mx.setter
    def mx(self, mx):
        self._mx = mx

    def forward(self, x, a, b):
        r"""Forward linear normalisation to a given range a, b.

        :arg x: Normalised data to be bounded
        
        :arg a: Lower bound of range

        :arg b: Upper bound of range

        :returns:

            :math: x_i^{\prime<t>} = (b - a)\frac{x_i^{<t>} - \textit{min}(x_i)}{\textit{max}(x_i)-\textit{min}(x_i)}+a

        :example: LinearNormal(mn=0, mx=100).forward(x=99, a=0, b=1)
        """
        mn = self.mn
        mx = self.mx
        return (b - a)*((x-mn)/(mx - mn))+a

    def backward(self, x, a, b):
        r"""Inversion of linear normalisation from a given range a, b.

        :arg x: Normalised data to be unbounded
        
        :arg a: Lower bound of range

        :arg b: Upper bound of range

        :returns:

            :math: x_i^{\prime<t>} = (b - a)\frac{x_i^{<t>} - \textit{min}(x_i)}{\textit{max}(x_i)-\textit{min}(x_i)}+a

        :example: LinearNormal(mn=0, mx=100).forward(x=0.99, a=0, b=1)
        """
        mn = self.mn
        mx = self.mx
        return (x - a)*((mx-mn)/(b-a))+mn
