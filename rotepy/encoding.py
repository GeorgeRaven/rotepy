# https://github.com/pytorch/examples/blob/e5f075f2ece6700c28fa730f8f23b4b0736a4074/word_language_model/model.py#L65-L105
import torch
import math


class PositionalEncoding(torch.nn.Module):
    r"""Inject some information about the relative or absolute position of the tokens in the sequence.
        torch.e positional encodings have the same dimension as the embeddings, so that the two can be summed.
        Here, we use sine and cosine functions of different frequencies.
    .. math:
        \text{PosEncoder}(pos, 2i) = sin(pos/10000^(2i/d_model))
        \text{PosEncoder}(pos, 2i+1) = cos(pos/10000^(2i/d_model))
        \text{where pos is the word position and i is the embed idx)
    Args:
        d_model: the embed dim (required)/ how many features (must be even).
        dropout: the dropout value (default=0.1).
        max_len: the max. length of the incoming sequence (default=5000).
    Examples:
        >>> pos_encoder = PositionalEncoding(d_model)
    """

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = torch.nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(
            0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        r"""Inputs of forward function
        Args:
            x: the sequence fed to the positional encoder model (required).
        Shape:
            x: [sequence length, batch size, embed dim]
            output: [sequence length, batch size, embed dim]
        Examples:
            >>> output = pos_encoder(x)
        """

        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

class ItPositionalEncoding(torch.nn.Module):
    """Iterative variant positional encoding.
    
    https://jamesmccaffrey.wordpress.com/2020/11/06/refactoring-the-pytorch-documentation-positionalencoding-class/
    an easier to debug and understand version of the non iterative version.
    Slower but only for initial generation that needs to be run once.
    """

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(ItPositionalEncoding, self).__init__()
        self.dropout = torch.nn.Dropout(p=dropout)

        # device = "cuda" if torch.cuda.is_available() else "cpu"
        pe = torch.zeros((max_len, 1, d_model),
                         dtype=torch.float32)  # .to(device)
        for pos in range(0, max_len):
            for i in range(0, d_model, 2):
                div_term = math.exp(i * -math.log(10000.0) / d_model)
                pe[pos, 0, i] = math.sin(pos * div_term)
                # this try catch handles odd number of features
                try:
                    pe[pos, 0, i+1] = math.cos(pos * div_term)
                except IndexError:
                    pass

        self.register_buffer('pe', pe)

    def forward(self, x):
        r"""Inputs of forward function
        Args:
            x: the sequence fed to the positional encoder model (required).
        Shape:
            x: [sequence length, batch size, embed dim]
            output: [sequence length, batch size, embed dim]
        Examples:
            >>> output = pos_encoder(x)
        """
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)
