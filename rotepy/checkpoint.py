import torch
import datetime
import logging
from rotepy.premonition import Premonition

def model_checkpoint(path: str,
                     model, optimizer, loss_function,
                     model_args=None, optimizer_args=None, loss_function_args=None,
                     **metadata):
    model_args = model_args if model_args is not None else {}
    optimizer_args = optimizer_args if optimizer_args is not None else {}
    loss_function_args = loss_function_args if loss_function_args is not None else {}
    #https://pytorch.org/tutorials/beginner/saving_loading_models.html#saving-loading-a-general-checkpoint-for-inference-and-or-resuming-training
    logging.debug("Saving checkpoint: {}".format(path))
    minimum_state = {
        "model_state_dict": model.state_dict(),
        "optimizer_state_dict": optimizer.state_dict(),
        # "loss_function_state_dict": loss_function.state_dict(),
        "datetime": datetime.datetime.utcnow(),
        "model_class": str(model.__class__.__name__),
        "optimizer_class": str(optimizer.__class__.__name__),
        "loss_class": str(loss_function.__class__.__name__),
        "model_args": model_args,
        "optimizer_args": optimizer_args,
        "loss_function_args": loss_function_args,
    }
    to_save_state = merge_dicts(minimum_state, metadata)
    torch.save(to_save_state, path)

def model_checkpoint_load(path: str = None):
    """Load a checkpoint from a given path."""
    checkpoint = torch.load(path)

    # MODELS
    # generate instance of model
    if checkpoint.get("model_class") == "Premonition":
        model = Premonition(**checkpoint["model_args"])
    else:
        raise ValueError("unsupported model class: '{}'".format(checkpoint.get("model_class")))
    model.load_state_dict(checkpoint["model_state_dict"])

    # OPTIMIZERS
    # generate instance of optimizer
    if checkpoint.get("optimizer_class") == "Adam":
        optimizer = torch.optim.Adam(model.parameters(), **checkpoint["optimizer_args"])
    else:
        raise ValueError("unsupported optimizer class: '{}'".format(checkpoint.get("optimizer_class")))
    optimizer.load_state_dict(checkpoint["optimizer_state_dict"])

    # LOSS FUNCTIONS
    # attain loss function
    if checkpoint.get("loss_class") == "MSELoss":
        print(checkpoint["loss_function_args"], type(checkpoint["loss_function_args"]))
        loss_fn = torch.nn.MSELoss(**checkpoint["loss_function_args"])
    else:
        raise ValueError("unsupported loss function class: '{}'".format(checkpoint.get("loss_class")))

    # # print out models state dictionary
    # print("Model's state_dict:")
    # for param_tensor in model.state_dict():
    #     print(param_tensor, "\t", model.state_dict()[param_tensor].size())
    # print()
    #
    # # print out optimizers state dictionary
    # print("Optimizer's state_dict:")
    # for var_name in optimizer.state_dict():
    #     print(var_name, "\t", optimizer.state_dict()[var_name])

    return model, optimizer, loss_fn, metadata


def merge_dicts(*dicts):
    result = {}
    for dictionary in dicts:
        result.update(dictionary)
    return result

