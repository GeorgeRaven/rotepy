import unittest
import torch
import torchvision
import time
import os
from modeler import Modeler

# GAFF MODEL
class NeuralNetwork(torch.nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = torch.nn.Flatten()
        self.linear_relu_stack = torch.nn.Sequential(
            torch.nn.Linear(28*28, 512),
            torch.nn.ReLU(),
            torch.nn.Linear(512, 512),
            torch.nn.ReLU(),
            torch.nn.Linear(512, 10)
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits

class ModelerTest(unittest.TestCase):
    """Confirm Modeler is working as expected."""

    def setUp(self):
        """Start timer and init variables."""
        self.start_time = time.time()

    def tearDown(self):
        """Calculate and print time delta."""
        t = time.time() - self.start_time
        print('%s: %.3f' % (self.id(), t))

    def test_mnist(self):
        """Ensuring Modeler is forward and backward proping correctly.

        Using MNIST a well known example, this test seeks to check if
        modeler is successfully implementing the torch api to learn
        digit recognition.
        """
        # Download training data from open datasets.
        training_data = torchvision.datasets.FashionMNIST(
            root="data",
            train=True,
            download=True,
            transform=torchvision.transforms.ToTensor(),
        )
        
        # Download test data from open datasets.
        test_data = torchvision.datasets.FashionMNIST(
            root="data",
            train=False,
            download=True,
            transform=torchvision.transforms.ToTensor(),
        )

        batch_size = 64

        # Create data loaders.
        train_dataloader = torch.utils.data.DataLoader(
                training_data,
                num_workers=1,
                batch_size=batch_size)
        test_dataloader = torch.utils.data.DataLoader(
                test_data,
                num_workers=1,
                batch_size=batch_size)
        
        # for X, y in test_dataloader:
        #     print(f"Shape of X [N, C, H, W]: {X.shape}")
        #     print(f"Shape of y: {y.shape} {y.dtype}")
        #     break

        # Get cpu or gpu device for training.
        device = "cuda" if torch.cuda.is_available() else "cpu"
        # print(f"Using {device} device")
        
        
        model = NeuralNetwork().to(device)
        path = "{}/checkpoints/MNIST-model.tar".format(os.getcwd())
        print(model)
        print(path)

        # LOSS FUNCTION
        loss_fn = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)
        scheduler_args = {
            "mode": "min",
        }
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, **scheduler_args)


        ml = Modeler()
        ml.model = model
        ml.model_args = {}

        ml.optimizer = optimizer
        ml.optimizer_args = {"lr": 1e-3}

        ml.scheduler = scheduler
        ml.scheduler_args = scheduler_args

        ml.loss_function = loss_fn
        ml.loss_function_args = {}

        ml.dataloader_training = train_dataloader
        ml.dataloader_validation = train_dataloader
        ml.dataloader_testing = test_dataloader

        print("Training ...")
        # example training with save
        ml.model_path = path
        ml.train(added_epoch=3, device=device)
        ml.save_checkpoint()

        print("Reloading & Training ...")
        # example further training with inference and loading
        ml.load_checkpoint()
        ml.train(added_epoch=3, device=device)

        print("Testing ...")
        ml.test(device=device)

        print("Forecasting ...")
        ml.test_trace(device=device)
