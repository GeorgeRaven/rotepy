import unittest
import time
import numpy as np
from normalisation import LinearNormal

class LinearNormTest(unittest.TestCase):
    """Check the historic clockhouse pytorch dataset works as expected."""

    def setUp(self):
        """Start timer and init variables."""
        self.start_time = time.time()

    def tearDown(self):
        """Calculate and print time delta."""
        t = time.time() - self.start_time
        print('%s: %.3f' % (self.id(), t))

    def test_forward(self):
        a, b=-1, 1
        x = np.arange(-10, 5, 1, dtype=int)
        n = LinearNormal(mn=np.min(x), mx=np.max(x)).forward(x=x, a=a, b=b)
        self.assertEqual(n[0], a)
        self.assertEqual(n[-1], b) 

    def test_backward(self):
        a, b=-1, 1
        x = np.arange(-10, 5, 1, dtype=int)
        norm = LinearNormal(mn=np.min(x), mx=np.max(x))
        n = norm.forward(x=x, a=a, b=b)
        nx = norm.backward(x=n, a=a, b=b)
        np.testing.assert_array_almost_equal(x, nx, decimal=1, verbose=True)

    def test_magic_run(self):
        a, b=-1, 1
        x = np.arange(-10, 5, 1, dtype=int)
        norm = LinearNormal(mn=np.min(x), mx=np.max(x))
        str(norm)
        norm.__setstate__(norm.__getstate__())

