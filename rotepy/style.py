# import os, sys
# import time
# import logging
# import pymongo
# import copy
# import numpy as np
# import pandas as pd
# import datetime
# from itertools import chain
# from ezdb.mongo import Mongo
# import matplotlib as mpl
# import matplotlib.pyplot as plt
# import seaborn as sns
# import datetime

import logging
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

palette = "husl"
sns.color_palette(palette, 10)
# seaborn customisations

matplotlib_rcparams = {
    "figure.figsize": (32/2, 18/2), # how big do we want the figures to be by default
    # "axes.facecolor": '#487890', #  default background to figure so can be seen around it
    "font.family": "monospace", # font for everything
    # "text.color": '#7890A8', # mostly just the title colour
    # "axes.edgecolor": '#7890A8', # border of central figure region colour
    # "figure.facecolor": '#004860', # central figure region colour
    "axes.labelsize": 26, # size of the non-numbered part of the labels on each axis
    "axes.titlesize": 32, # size of the title
    "font.size": 26,
    "legend.title_fontsize": 28,
    "legend.fontsize": 24,
    "xtick.labelsize": 24,
    "ytick.labelsize": 24,
    # "xtick.color": '#7890A8',
    # "ytick.color": '#7890A8',
    # "xtick.labelcolor": '#7890A8', # x axis colouring
    # "ytick.labelcolor": '#7890A8',
    # "axes.labelcolor": '#7890A8',
    # "legend.facecolor": '#183048',
    "lines.linewidth": 3,
}
# matplotlib_rcparams = {
#     "figure.figsize": (32, 18), # how big do we want the figures to be by default
#     "axes.facecolor": '#487890', #  default background to figure so can be seen around it
#     "font.family": "monospace", # font for everything
#     "text.color": '#7890A8', # mostly just the title colour
#     "axes.edgecolor": '#7890A8', # border of central figure region colour
#     "figure.facecolor": '#004860', # central figure region colour
#     "axes.labelsize": 26, # size of the non-numbered part of the labels on each axis
#     "axes.titlesize": 32, # size of the title
#     "font.size": 26,
#     "legend.title_fontsize": 28,
#     "legend.fontsize": 24,
#     "xtick.labelsize": 24,
#     "ytick.labelsize": 24,
#     "xtick.color": '#7890A8',
#     "ytick.color": '#7890A8',
#     "xtick.labelcolor": '#7890A8', # x axis colouring
#     "ytick.labelcolor": '#7890A8',
#     "axes.labelcolor": '#7890A8',
#     "legend.facecolor": '#183048',
#     "lines.linewidth": 3,
# }
sns.set_theme(style="darkgrid")
sns.set(rc=matplotlib_rcparams)
sns.set_palette(palette, 10)
# [(parm, value) for parm, value in mpl.rcParams.items() if "size" in parm]
